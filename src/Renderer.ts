export default interface Renderer {
    render(): void;
}
