import Provider from "./Provider";
import DIContainer from "rsdi";
import Renderer from "./Renderer";

class Application {
    providerClasses: typeof Provider[] = [];
    providers: Provider[] = [];
    providersAddedDuringRegister: Provider[] = [];
    container: DIContainer;
    booting: boolean = false;

    constructor() {
        const container = new DIContainer();
        container.add({'Application': this});
        this.container = container;
    }

    make<T>(name: string) {
        return this.container.get<T>(name);
    }

    boot(): void {
        this.booting = true;
        this.prepareProviders();
        this.registerProviders();
        this.bootProviders();
        this.booting = false;
    }

    render() {
        this.container.get<Renderer>('Renderer').render();
    }

    addProvider(providerClass: typeof Provider) {
        this.providerClasses.push(Provider);
        if(this.booting) {
            const provider: Provider = this.instantiateProvider(providerClass);
            this.providers.push(provider);
            this.providersAddedDuringRegister.push(provider);
        }
    }

    setProviders(providers: typeof Provider[]) {
        this.providerClasses = providers
    }

    prepareProviders(): void {
        this.instantiateProviders();
    }

    registerProviders(): void {
        this.providers.forEach((provider: Provider) => {
            provider.register();
        });

        this.handleProvidersAddedDuringRegisterRuns();
    }

    bootProviders(): void {
        this.providers.forEach((provider: Provider) => {
            provider.boot();
        });
    }

    instantiateProviders(): void {
        this.providers = this.providerClasses.map((providerType: typeof Provider) => {
            return this.instantiateProvider(providerType);
        });
    }

    handleProvidersAddedDuringRegisterRuns() {
        while( this.hasProvidersAddedDuringLastRegisterRun() ) {
            this.resetAndLoopProvidersAddedDuringLastRegisterRun((provider: Provider) => {
                provider.register();
            });
        }
    }

    instantiateProvider(providerType: typeof Provider): Provider {
        return new providerType(this.container);
    }

    hasProvidersAddedDuringLastRegisterRun(): boolean {
        return this.providersAddedDuringRegister.length > 0;
    }

    resetAndLoopProvidersAddedDuringLastRegisterRun(callback: (provider: Provider) => void) {
        let providersAddedDuringRegister = this.providersAddedDuringRegister;
        this.providersAddedDuringRegister = [];
        providersAddedDuringRegister.forEach(callback);
    }

}

export default Application;
