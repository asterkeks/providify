import Container from "rsdi";

class Provider {
    container: Container;

    constructor(container: Container) {
        this.container = container;
    }


    boot(): void {
    }

    register(): void {
    }

    setContainer(container: Container) {
        this.container = container;
    }
}

export default Provider;
