# providify

[![Build Status](https://drone.asterkeks.de/api/badges/cmd-keen/providify/status.svg)](https://drone.asterkeks.de/cmd-keen/providify)

basic application start code with di container and providers to interact with the container on application boot

## Install

Providify does not do anything node or browser specific an thus can be used in either environment

For the browser you generally want to only have it in your dev dependencies and compile it into your single page app:

    npm i --save-dev providify

In node you probably don't compile your application so you want to have it in your production dependencies:

    npm i --save providify

## Usage

The strenght of providify lies in the Providers which are registered, then booted. To give them something to interact
with they receive the dependency injection container rsdi

```typescript
import Application, {Provider} from 'providify';

class ConfigProvider extends Provider {
    register() {
        this.container.add({'config': {
            'database': 'localhost:3306'
        }});
    }
}

const app = new Applicication();

app.setProviders([
    ConfigProvider
]);

app.boot();

app.render();
```

### Providers

Providers receive the rsdi DI Container as `this.container`. Then all providers' `register` functions are run. Lastly all
providers' `boot` functions are run.

Generally you want to use the `register` function of your provider to fill the DI Container. Then act on the content of
the DI Container in the `boot` function. This gives other Providers the chance to replace the DI Container entry but all
code working with this entry receiving the correct instance

```typescript
import {Provider} from 'providify';
import BasicDatabase from './BasicDatabase';
import CoolNewDatabase from './CoolNewDatabase';

interface Database {
    select(table: string): Array;
    setConfig(config: Config);
}

class DatabaseProvider extends Provider {
    register() {
        this.container?.add({'Database', object(BasicDatabase)});
    }
    
    boot() {
        // the config will be set on an object of CoolNewDatabase
        this.container?.get<Database>('Database').setConfig({
            'database': 'localhost:3306'
        });
    }
}

class CoolNewDatabaseProvider extends Provider {
    register() {
        this.container?.addDefinition({'Database': object(CoolNewDatabase)});
    }
}

...
app.setProviders([
    DatabaseProvider,
    CoolNewDatabaseProvider
]);
...
```