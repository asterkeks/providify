import Application, {Provider, Renderer} from "../src/index";
import DIContainer, {object} from "rsdi";

describe('Application', function () {
    function bootApplication(providers: typeof Provider[]): Application {
        const app: Application = new Application();

        app.setProviders(providers);

        app.boot();

        return app;
    }

    it('passes container to Provider', () => {
        class TestProvider extends Provider {
            static container: DIContainer;

            constructor(container: DIContainer) {
                super(container);
                TestProvider.container = container;
            }
        }

        const app = bootApplication([
            TestProvider
        ])


        expect(TestProvider.container).toBe(app.container);
    });

    it('registers provider', () => {

        class TestProvider extends Provider {
            static wasCalled: boolean = false;

            register() {
                TestProvider.wasCalled = true;
            }

        }

        bootApplication([
            TestProvider
        ])


        expect(TestProvider.wasCalled).toBeTruthy();
    });

    it('boots provider', () => {

        class TestProvider extends Provider {
            static wasCalled: boolean = false;

            boot() {
                TestProvider.wasCalled = true;
            }

        }

        bootApplication([
            TestProvider
        ])


        expect(TestProvider.wasCalled).toBeTruthy();
    });

    it('render defers to Renderer', () => {

        class TestRenderer implements Renderer {
            static wasCalled: boolean = false;

            render(): void {
                TestRenderer.wasCalled = true;
            }

        }

        const app: Application = new Application();
        app.container.add({'Renderer': object(TestRenderer)});
        app.render();

        expect(TestRenderer.wasCalled).toBeTruthy();
    });

    it('provides itself in the container', () => {

        const app: Application = new Application();

        expect(app.container.get<Application>('Application')).toStrictEqual(app);
    });

    it('can add other Provider during register which is registered and booted', () => {
        class TestProvider extends Provider {
            register() {
                this.container?.get<Application>('Application').addProvider(LateProvider);
            }
        }

        class LateProvider extends Provider {
            static registered: boolean = false;
            static booted: boolean = false;
            static container: DIContainer|false = false;

            constructor(container: DIContainer) {
                super(container);
                LateProvider.container = container;
            }


            register() {
                LateProvider.registered = true;
            }

            boot() {
                LateProvider.booted = true;
            }
        }

        const app = bootApplication([
            TestProvider
        ])

        expect(LateProvider.registered).toBeTruthy();
        expect(LateProvider.booted).toBeTruthy();
        expect(LateProvider.container).toBe(app.container);
    });

    it('can add other provider recursively', () => {
        class TestProvider extends Provider {
            register() {
                this.container?.get<Application>('Application').addProvider(IntermediateProvider);
            }
        }

        class IntermediateProvider extends Provider {
            register() {
                this.container?.get<Application>('Application').addProvider(LateProvider);
            }
        }

        class LateProvider extends Provider {
            static registered: boolean = false;
            static booted: boolean = false;
            static container: DIContainer|false = false;

            constructor(container: DIContainer) {
                super(container);
                LateProvider.container = container;
            }


            register() {
                LateProvider.registered = true;
            }

            boot() {
                LateProvider.booted = true;
            }
        }

        const app = bootApplication([
            TestProvider
        ])

        expect(LateProvider.registered).toBeTruthy();
        expect(LateProvider.booted).toBeTruthy();
        expect(LateProvider.container).toEqual(app.container);
    });

    it('can access the container through app.make', function () {
        class TestProvider extends Provider {
            register() {
                this.container.add({
                    test: 'found'
                });
            }
        }

        const app = bootApplication([
            TestProvider
        ])

        expect(app.make<string>('test')).toBe('found');
    });
});
