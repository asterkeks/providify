import {Provider} from "../src/index";
import DIContainer from "rsdi";

test('Application passes container to Provider', () => {

    const container = new DIContainer();
    const provider = new Provider(container);

    expect(provider.container).toStrictEqual(container);
});